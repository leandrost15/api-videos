export function define_create_table(table,columns){
  return `CREATE TABLE ${table} ( ${columns.join(",")} )`
}

//create_table("db",["titulo char(200)","path_video char(200)","desc text"])

export function define_insert_table(table,values){
  return `INSERT INTO ${table} VALUES ("${values.join('",'+'"')}")`
}

//insert_table("db",["cafeina","mustache","morango"])

