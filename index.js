import express from 'express'
import fileUpload from 'express-fileupload'
import {define_create_table,define_insert_table} from './resources/controller/database.js'
import crypto from 'crypto';
import { conn } from './resources/model/database.js'
import fs from 'fs'
const app = express()

app.use(fileUpload())

conn().then((pointer) => {
  pointer.run(
    define_create_table("links",["titulo char(200)","path_video char(200)","desc text"])
  ).catch((err) => {
    if (err.errno == 1) "ok"
  })
})

function random_name(){
  let current_date = (new Date()).valueOf().toString();
  let random = Math.random().toString();  
  return crypto.createHash('sha1').update(current_date + random).digest('hex') 
}

app.post('/upload', function(req, res) {
  let filepath = 'default.jpeg'

  if(req.files){
    let file = req.files.filetoupload;

    if(file.mimetype.indexOf("video") != -1 || file.mimetype.indexOf("image") != -1){
      filepath = `${random_name()}.${file.mimetype.split("/")[1]}`
      file.mv(`./repositorio/${filepath}`, err => {
        if(err) res.send(err)
      })
    }
  }
  conn().then((pointer) => {
    pointer.run(
      define_insert_table("links",[req.body.titulo,filepath,req.body.desc])
    ).then((resp) => {
      res.send("Arquivo foi carregado!")
    }, err => {
      res.send(err)
    })
  })  
});

app.get('/watch',(req,res)=>{
  res.writeHead(200, {"Content-Type": "video/mp4"})
  var rs = fs.createReadStream(`./repositorio/${req.query.filename}`)
  rs.pipe(res)
})

app.get('/datafiles',(req,res)=>{
  conn().then((pointer) => {
    pointer.all("select * from links").then((dbres) => {
      res.send(dbres)
    }, err => {
      res.send(err)
    })
  })
})
app.listen(3000)
